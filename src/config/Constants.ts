export const AppModuleName = 'vortex.planner';
export const CardModuleName = 'vortex.planner.card';
export const CardComponentName = 'card';
export const CardCtrlHandle = 'cardCtrl';