 import {module} from 'angular';
 import { CardComponent } from './components/card.component';
 import { CardComponentName, CardModuleName } from './config/Constants';
 
 export default module(CardModuleName, [])
        .component(CardComponentName, new CardComponent());