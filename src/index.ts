import { module, bootstrap } from 'angular';
import './app.module';
import { CardModuleName, AppModuleName } from './config/Constants';

import 'ng-office-ui-fabric';

module(AppModuleName,
    [
        'officeuifabric.core',
        'officeuifabric.components',
        CardModuleName
    ]);

bootstrap(document.getElementById('container'), [AppModuleName], {
    strictDi: true
});