import { CardController } from './card.controller';
import { CardCtrlHandle } from '../config/Constants';
var template = require('./card.component.html');

/**
 * CardComponent
 */
/**
    * Alert Body component.
    */
export class CardComponent implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public templateUrl: string;
    public template: string;
    public controllerAs: string;
    public transclude: boolean;

    constructor() {
        this.controller =
            [
                CardController
            ];        
        this.templateUrl = template;
        this.controllerAs = CardCtrlHandle ;
        this.bindings = {
            title: '@'
        };
        this.transclude = true;
    }
}

